# LinkedIn Scraper

### This tool is for education and research use only!

## Introduction

This tool provides capability of retrieving the full personal profile in JSON object when given the profile address.

LinkedIn's authentication check includes both session ID and IP address. A containerized virtual environment rotates dynamic IP addresses. Therefore, this setup is limited to local host only.

## Get Started

* ### Prerequisite
Make sure [nodejs](https://nodejs.org/en/download/) is installed locally. 

* ### Install
To install dependencies
```bash
$ npm install
```

* ### Run
To run application
```bash
$ node scraper.js
```

## How to Use

Query personal profile in the following format, where url is the personal LinkedIn profile link address, and session is the _**new**_ `li_at` cookie value after you log in.

```
<host address>?url=<linkedin profile address>&session=<linkedin session id>
```

Example query:

```bash
http://localhost:3000/?url=https://www.linkedin.com/in/erichfeige/&session=AQEDAQhZlcEBaSseAAABctngNVoAAAF5dmWSWE4AI9hzCl8WNcjSjx-SMLqG8_hbIugSccD4h1MA8Sk61UaVKYMJMzbrXz5xUcHsHoQjk_l-ZRKCRqrAH-t2xmFEZez1tOajjAiVWiTVbCiUQiKiVMxt
```
