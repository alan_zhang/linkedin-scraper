
require('dotenv').config();

const linkedin = require('linkedin-profile-scraper')
const app = require('express')();

(async () => {

    // Usage: http://localhost:3000/?url=https://www.linkedin.com/in/erichfeige/&session=AQEDAQhZlcEBaSseAAABctngNVoAAAF5dmWSWE4AI9hzCl8WNcjSjx-SMLqG8_hbIugSccD4h1MA8Sk61UaVKYMJMzbrXz5xUcHsHoQjk_l-ZRKCRqrAH-t2xmFEZez1tOajjAiVWiTVbCiUQiKiVMxt
    app.get('/', async (req, res) => {
        // Get query params
        const urlToScrape = req.query.url;
        const session = req.query.session;

        // Setup environment variables to fill the sessionCookieValue
        const scraper = new linkedin.LinkedInProfileScraper({
            sessionCookieValue: session,
            keepAlive: true,
        })
        // Prepare the scraper
        // Loading it in memory
        await scraper.setup()

        const result = await scraper.run(urlToScrape)
        return res.json(result)
    })

    app.listen(process.env.PORT || 3000)
})()
