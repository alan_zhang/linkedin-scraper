FROM node:14-alpine

WORKDIR /app

COPY . /app

RUN npm ci

EXPOSE 3000

CMD ["node", "scraper.js"]
